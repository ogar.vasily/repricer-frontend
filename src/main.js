import Vue from 'vue'
import VueRouter from 'vue-router'
import products from './components/products.vue'
import product from './components/product.vue'
import imports from './components/imports.vue'
import add_import from './components/add_import.vue'
import suppliers from './components/suppliers.vue'
import supplier from './components/supplier.vue'
import warehouses from './components/warehouses.vue'
import warehouse from './components/warehouse.vue'
//import general_settings from './components/general_settings.vue'
import attributes from './components/attributes.vue'
import attribute from './components/attribute.vue'
import VueResource from 'vue-resource'

Vue.use(VueResource);
Vue.use(VueRouter);

var router = new VueRouter();

// Define some routes.
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
router.map({
    /* Products */
    '/products': {
        component: products
    },
    '/add-product': {
        component: product
    },
    '/product/:id': {
        component: product
    },
    /* Imports */
    '/imports': {
        component: imports
    },
    '/add-import': {
        component: add_import
    },
    '/import/:id': {
        component: add_import
    },
    /* Suppliers */
    '/suppliers': {
        component: suppliers
    },
    '/add-supplier': {
        component: supplier
    },
    'supplier/:id': {
        component: supplier
    },
    /* Warehouses */
    '/warehouses': {
        component: warehouses
    },
    '/add-warehouse': {
        component: warehouse
    },
    'warehouse/:id': {
        component: warehouse
    },
    //'/general': {
    //    component: general_settings
    //},
    /* Attributes */
    '/attributes': {
        component: attributes
    },
    '/add-attribute': {
        component: attribute
    },
    '/attribute/:id': {
        component: attribute
    },
});

var App = Vue.extend();
// Now we can start the app!
// The router will create an instance of App and mount to
// the element matching the selector #app.
router.start(App, '#app');